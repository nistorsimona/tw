import axios from 'axios'

const SERVER = 'https://final-simonanistor.c9users.io'

class VideoStore{
  constructor(ee){
    this.ee = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(SERVER + '/videos/')
      .then((response) => {
        this.content = response.data
        this.ee.emit('VIDEO_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(video){
    axios.post(SERVER + '/videos/', video)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(SERVER + '/videos/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, video){
    axios.put(SERVER + '/videos/' + id, video)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(SERVER + '/videos/' + id)
      .then((response) => {
        this.selected = response.data
        this.ee.emit('SINGLE_VIDEO_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default VideoStore
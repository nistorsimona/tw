import React, {Component} from 'react'
import VideoStore from '../stores/VideoStore'
import {EventEmitter} from 'fbemitter'
//import Video from './Video'
import VideoForm from './VideoForm'

const ee = new EventEmitter()
const store = new VideoStore(ee)

class PlaylistDetails extends Component{
  constructor(props){
    super(props)
    this.state = {
      videos : []
    }
    this.addVideo = (video) => {
      store.addOne(this.props.playlist.id, video)
    }
  }
  componentDidMount(){
    store.getAll(this.props.playlist.id)
    ee.addListener('VIDEO_LOAD', () => {
      this.setState({
        videos : store.content
      })
    })
  }
  render(){
    return (
      <div>
        currently editing {this.props.playlist.title}
        <h3>list of videos</h3>
        {this.state.videos.map(video => {
        return ( <div key={video.id} >
                 <h3>Video-ul {video.title} se poate accesa la &nbsp;
                 {video.link} </h3></div>)})}
        <h3>add a new one</h3>
        <VideoForm onAdd={this.addVideo}/>
      </div>  
    )
  }
}

export default PlaylistDetails
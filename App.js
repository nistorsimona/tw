import React, { Component } from 'react'
import '../styles/App.css'
import Playlists from './Playlists'
//import Video from './Video'
//import PlaylistForm from './PlaylistForm'
//import Videos from './Videos'
import Vids from './1s'
//import ShowP from './ShowP'

/*import YouTubeChannel from './node_modules/react-youtube-playlist-masterreact-youtube-channel';
import 'reacty-youtube-playlist/dist/styles'

const App = () => {
  return (
    <YouTubePlaylist
      width={'85%'}
      height={390}
      api_key='YourGoogleApiKey'
      playlist_id='YourYoutubePlaylistID'
      show_thumbnails
    />
  )
}*/

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">YouTube Video Manager</h1>
        </header>
        <Playlists/>
        <div className="EmptyDiv"></div>
        <Vids/>
      </div>
    )
  }
}

export default App

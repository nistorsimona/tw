import React, { Component } from 'react'

class Playlist extends Component {
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      title : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] :event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      title : this.props.playlist.title
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({isEditing : false})
  }
  render() {
    if (!this.state.isEditing){
      return (
        <div>
          {this.props.playlist.title} with id {this.props.playlist.id}
          <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.playlist.id)}/>
          <input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
          <input type="button" value="details" onClick={() => this.props.onSelect(this.props.playlist.id)} />
        </div>
      )
    }
    else{
      return (
        <div>
          <input type="text" name="playlistTitle" onChange={this.handleChange} value={this.state.playlistTitle}/>
          <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
          <input type="button" value="save" onClick={
            () => this.props.onSave(this.props.playlist.id, {
                name : this.state.playlistTitle
            })} />
        </div>
      )
    }
  }
}

export default Playlist

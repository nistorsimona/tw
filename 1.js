import React, { Component } from 'react'

class Vid extends Component {
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      title : '',
      description : '',
      category: '',
      link:''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] :event.target.value
      })
    }
  }
  componentDidMount(){
    this.setState({
      title : this.props.video.title,
      description : this.props.video.description,
      category : this.props.video.category,
      link : this.props.video.link
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({isEditing : false})
  }
  render() {
    if (!this.state.isEditing){
      return (
        <div>
          {this.props.video.title} with id {this.props.video.id}, description {this.props.video.description}, category {this.props.video.category},
          link {this.props.video.link}
        </div>
      )
    }
    else{
      return (
        <div>
          <input type="text" name="title" onChange={this.handleChange} value={this.state.title}/>
          <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
          <input type="button" value="save" onClick={
            () => this.props.onSave(this.props.video.id, {
                title : this.state.title,
                description : this.state.description,
                category : this.state.category,
                link : this.state.link
            })} />
        </div>
      )
    }
  }
}

export default Vid

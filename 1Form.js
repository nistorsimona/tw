import React, { Component } from 'react'

class VidForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      title : '',
      description : '',
      category: '',
      link:''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.title] :event.target.value,
        [event.target.description] :event.target.value,
        [event.target.category] :event.target.value,
        [event.target.link] :event.target.value
      })
    }
  }
  render() {
    return (
      <div>
        Title : <input type="text" title="title" onChange={this.handleChange}/>
        Description : <input type="text" description="description" onChange={this.handleChange}/>
        Category : <input type="text" category="category" onChange={this.handleChange}/>
        Link : <input type="text" link="link" onChange={this.handleChange}/>
        <input type="button" value="add" onClick={() => this.props.onAdd({title : this.state.title, description : this.state.description,
          category : this.state.category, link : this.state.link
        })} />
      </div>
    )
  }
}

export default VidForm
import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import Video from './Video'
import VideoForm from './VideoForm'
import SingleVideo from '../stores/SingleVideo'
//import Details from './PlaylistDetails'

const ee = new EventEmitter()
const store = new SingleVideo(ee)

function addVideo(video){
  //console.log(video.title)
  store.addOne(video)
}

function deleteVideo(id){
  store.deleteOne(id)
}

function saveVideo(id, video){
  store.saveOne(id, video)
}

class Videos extends Component {
  constructor(props){
    super(props)
    this.state = {
      videos : [],
      detailsFor : 0,
      selected : null
    }
    this.selectVideo = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_VIDEO_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('VIDEO_LOAD', () => {
      this.setState({
        videos : store.content
      })
    })
  }
  render() {
    if(!this.state.detailsFor){
      return (
        <div>
          <div>
            List of videos
            {
              this.state.videos.map((p) => <Video Video={p} key={p.id} onDelete={deleteVideo} onSave={saveVideo} onSelect=
              {this.selectVideo} />)
            }
          </div>
          <div>
            <VideoForm onAdd={addVideo}/>
          </div>
        </div>
      )      
    }
    else{
      return (
        <div>
          under construction
          <input type="button" value="back" onClick={() => this.setState({detailsFor : 0})} />
          
        </div>  
      )
    }

  }
}

export default Videos

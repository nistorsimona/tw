import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import Playlist from './Playlist'
import PlaylistForm from './PlaylistForm'
import PlaylistStore from '../stores/PlaylistStore'
import PlaylistDetails from './PlaylistDetails'

const ee = new EventEmitter()
const store = new PlaylistStore(ee)

function addPlaylist(playlist){
  console.log(playlist.title)
  store.addOne(playlist)
}

function deletePlaylist(id){
  store.deleteOne(id)
}

function savePlaylist(id, playlist){
  store.saveOne(id, playlist)
}

class Playlists extends Component {
  constructor(props){
    super(props)
    this.state = {
      playlists : [],
      detailsFor : 0,
      selected : null
    }
    this.selectPlaylist = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_PLAYLIST_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('PLAYLIST_LOAD', () => {
      this.setState({
        playlists : store.content
      })
    })
  }
  render() {
    if(!this.state.detailsFor){
      return (
        <div>
          <div>
            List of playlists
            {
              this.state.playlists.map((p) => <Playlist playlist={p} key={p.id} onDelete={deletePlaylist} onSave={savePlaylist} onSelect=
              {this.selectPlaylist} />)
            }
          </div>
          <div>
            <PlaylistForm onAdd={addPlaylist}/>
          </div>
        </div>
      )      
    }
    else{
      return (
        <div>
          under construction
          <input type="button" value="back" onClick={() => this.setState({detailsFor : 0})} />
          <PlaylistDetails playlist={this.state.selected} />
        </div>  
      )
    }

  }
}

export default Playlists

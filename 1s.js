import React, { Component } from 'react'
import {EventEmitter} from 'fbemitter'
import Video from './1'
import VidForm from './1Form'
import VidStore from '../stores/1Store'
//import PlaylistDetails from './PlaylistDetails'

const ee = new EventEmitter()
const store = new VidStore(ee)

function addVideo(video){
  //console.log(video.title)
  store.addOne(video)
}

function deleteVideo(id){
  store.deleteOne(id)
}

function saveVideo(id, video){
  store.saveOne(id, video)
}

class Vids extends Component {
  constructor(props){
    super(props)
    this.state = {
      videos : [],
      detailsFor : 0,
      selected : null
    }
    this.selectVideo = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_VID_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('VID_LOAD', () => {
      this.setState({
        videos : store.content
      })
    })
  }
  render() {
      return (
        <div>
          <div>
            List of vids
            {
              this.state.videos.map((p) => <Video video={p} key={p.id} onDelete={deleteVideo} onSave={saveVideo} onSelect=
              {this.selectVideo} />)
            }
          </div>
          <div>
            <VidForm onAdd={addVideo}/>
          </div>
        </div>
      )      
    }
}

export default Vids

import React, {Component} from 'react'

class Video extends Component{
  render(){
    return (
      <div>
        <h5>{this.props.video.title}</h5>
        <h5>{this.props.video.description}</h5>
        <h5>{this.props.video.category}</h5>
        <h5>{
          this.props.video.link
        }</h5>
      </div>
    )
  }
}

export default Video
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('sequelize_manager','root','',{
  dialect : 'mysql',
  port : 3306
})



var port = process.env.PORT || 8083
console.log(port)

const Playlist = sequelize.define('playlist', {
    title : {
      type : Sequelize.STRING,
      allowNull : false
    }
})

const Video = sequelize.define('video', {
    title: {
      type : Sequelize.STRING,
      allowNull : false
    },
    description : {
        type : Sequelize.TEXT,
        allowNull : false
    },
    category : {
      type : Sequelize.STRING,
      allowNull: false
    },
    link : {
        type : Sequelize.STRING,
        allowNull: false
    }
})

//Playlist.hasMany(Video, {foreignKey: 'videoID'})
//Video.belongsTo(Playlist, {foreignKey: 'videoID'})
Playlist.belongsToMany(Video, {through: 'playlist_video'})
Video.belongsToMany(Playlist,{through: 'playlist_video'})

const app = express()
app.use(bodyParser.json())
//app.use(express.static('app'))

app.get('/create', function(req, res){
    sequelize.sync({force: true})
        .then(function(){
            res.status(201).send('created')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.get('/playlists', function(req, res) {
    Playlist.findAll({attributes : ['id', 'title']})
        .then(function(playlists){
            res.status(200).send(playlists)
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.post('/playlists', function(req, res) {
    console.log(req.body)
    Playlist.create(req.body)
        .then(function(){
            res.status(201).send('created')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.get('/playlists/:id', function(req, res) {
    Playlist.find({where : {id : req.params.id}, attributes : ['id', 'title']})
        .then(function(playlist){
            if (playlist){
                res.status(200).send(playlist)
            }
            res.status(404).send('not found')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.put('/playlists/:id', function(req, res) {
    Playlist.find({where : {id : req.params.id}})
        .then(function(playlist){
            return playlist.updateAttributes(req.body)
        })
        .then(function(){
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.delete('/playlists/:id', function(req, res) {
    Playlist.find({where : {id : req.params.id}})
        .then(function(playlist){
            return playlist.destroy()
        })
        .then(function(){
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.get('/playlists/:id/', function(req, res) {
    Playlist.find({where : {id : req.params.id}, include : [Video]})
        .then(function(playlist) {
            return playlist.getVideos()
        })
        .then(function(videos) {
            res.status(200).send(videos)
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.post('/playlists/:id/videos', function(req, res) {
    Playlist.find({where : {id : req.params.id}})
        .then(function(video) {
            return Video.create({
                title : req.body.title,
                description: req.body.description,
                category: req.body.category,
                videoId : video.id,
                link: req.body.link
            })
        })
        .then(function() {
            res.status(201).send('created')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        }) 
})

/*app.get('/playlists/:id/videos', function(req, res) {
    Playlist.find({where : {id : req.params.id, id: req.params.videoId}, include : [Video]})
        .then(function(playlist) {
            return playlist.getVideos()
        })
        .then(function(videos) {
            res.status(200).send(videos)
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})*/

app.put('/playlists/:id/videos/vId', function(req, res) {
    Video.find({where : {id : req.params.id}})
        .then(function(video){
            video.title = req.body.title
            video.description = req.body.title;
            video.category = req.body.category;
            video.link = req.body.link;
            return video.save(['title', 'description', 'category','link'])
        })
        .then(function() {
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.delete('/playlists/:id/videos/vId', function(req, res) {
    Video.find({where : {id : req.params.id}})
        .then(function(video){
            return video.destroy()
        })
        .then(function() {
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

/*
app.get('/videos/:id/playlists', (req, res, next) => {
	Video.findById(req.params.id)
		.then((video) => {
			if (video){
				return video.getPlaylists()
			}
			else{
				res.status(404).send('not found')
			}
		})
		.then((playlists) => res.status(200).send(playlists))
		.catch((err) => next(err))
})*/

app.get('/videos', function(req, res) {
    Video.findAll({attributes : ['id','title','description','category','link']})
        .then(function(videos){
            res.status(200).send(videos)
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.post('/videos', function(req, res) {
    console.log(req.body)
    Video.create(req.body)
        .then(function(){
            res.status(201).send('created')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.put('/videos/:id', function(req, res) {
    Video.find({where : {id : req.params.id}})
        .then(function(video){
            return video.updateAttributes(req.body)
        })
        .then(function(){
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.delete('/videos/:id', function(req, res) {
    Video.find({where : {id : req.params.id}})
        .then(function(video){
            return video.destroy()
        })
        .then(function(){
            res.status(200).send('modified')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.get('/videos/:id', function(req, res) {
    Video.find({where : {id : req.params.id}, attributes : ['id','title','description','category','link']})
        .then(function(video){
            if (video){
                res.status(200).send(video)
            }
            res.status(404).send('not found')
        })
        .catch(function(error){
            console.warn(error)
            res.status(400).send('error')
        })
})

app.use((err,req,res,next) => {
  console.warn(err)
  res.status(500).send('some error')
})

app.listen(port)
//console.log(port)
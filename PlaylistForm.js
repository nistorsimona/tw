import React, { Component } from 'react'

class PlaylistForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      playlistTitle : ''
    }
    this.handleChange = (event) => {
      this.setState({
        [event.target.title] :event.target.value
      })
    }
  }
  render() {
    return (
      <div>
        Title : <input type="text" title="playlistTitle" onChange={this.handleChange}/>
        <input type="button" value="add" onClick={() => this.props.onAdd({title : this.state.playlistTitle})} />
      </div>
    )
  }
}

export default PlaylistForm
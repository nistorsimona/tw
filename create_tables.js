const Sequelize = require('sequelize')

const sequelize = new Sequelize('sequelize_manager','root','',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

  /*getterMethods: {
    fullName() {
      return this.firstname + ' ' + this.lastname
    }
  },
  setterMethods: {
    fullName(value) {
      const names = value.split(' ')
      this.setDataValue('firstname', names.slice(0, -1).join(' '))
      this.setDataValue('lastname', names.slice(-1).join(' '))
    },
  }*/


const Playlist = sequelize.define('playlist', {
    title : {
      type : Sequelize.STRING,
      allowNull : false,
      validate : {
        isTitle : true
      }
    }
})

const Video = sequelize.define('video', {
    title: {
      type : Sequelize.STRING,
      allowNull : false,
      valitate : {
        len : [5,20]
      }
    },
    description : {
        type : Sequelize.TEXT,
        allowNull : false,
        validate : {
            len : [5,1000]
        }
    },
    category : {
      type : Sequelize.STRING,
      allowNull: false,
      validate : {
        isCategory : true
      }
    },
    link : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            isLink : true
        }
    }
})

sequelize.sync({force : true})
	.then(() => console.log('created'))
	.catch((error) => console.log(error))

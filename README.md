# README #

MANAGER VIDEOCLIPURI FAVORITE INTEGRAT CU YOUTUBE

Avand peste un miliard de utilizatori si milioane de videoclipuri incarcate saptamanal, YouTube va avea pentru fiecare utilizator cel putin un videoclip pe care va dori sa il vada de mai multe ori. Pentru asemenea videoclipuri exista posibilitatea de adaugare intr-o lista de redare (playlist) "favorite" cu scopul de a regasi continutul favorit de pe YouTube cu mai multa usurinta.
Asadar, aplicatia are ca scop crearea de multiple liste de redare de videoclipuri favorite ce pot fi modificate cu usurinta si propune urmatoarele:

-> creare playlist / redenumire playlist / stergere playlist / reordonarea listei de videoclipuri
-> "ohno" prevention - salvarea titlurilor de videoclipuri pentru a preveni pierderea favoritelor in cazul in care videoclipurile sunt sterse si sugerarea unui videoclip asemanator in acest caz
-> statistici de vizualizare a videoclipurilor / numar de vizualizari / tendinte
